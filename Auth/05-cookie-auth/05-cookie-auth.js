const express = require('express');

const app = express();
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'pug')

const auth = require('./05-auth-middleware');
auth.initCookieAuth(app);
app.use(auth.getUserFromCookie);

app.get('/',  function (req, res) {
    res.render('index', {user : req.user});
});

app.get('/login', function (req, res) {
    res.render('login');
});

app.post('/login',  function (req, res) {    
    var username = req.body.username;
	var password = req.body.password;
    if (password !== 'some password') {
        res.render('login');
    }
    else {
        auth.signInUser(res, username);

        let returnUrl = req.query.returnUrl;
        if (returnUrl !== undefined) {
            res.redirect(returnUrl); //TO DO: (Security) Check if this is a local url
        }        
        else {
            res.redirect("/");
        }
    }
});

app.post('/logout',   function (req, res) {
    auth.signOutUser(res);
    res.redirect("/");
});

app.get('/private', function (req, res) {       
    if (req.user.isAuthenticated) {                
        if (req.user.username.toLowerCase() === 'alice' || req.user.username.toLowerCase() === 'bob') {
            res.render('private', {username : req.user.username});
        }
        else {
            res.status(403);
            res.end('Forbidden for ' + req.user.username);
        }
    } 
    else {
        res.redirect(302, 'login?returnUrl=/private');     
    }       
});

const hostname = '127.0.0.1';
const port = 4050;
app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
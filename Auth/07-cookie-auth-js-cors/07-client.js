const express = require('express');
const app = express();
app.set('view engine', 'pug')

app.get('/', function (req, res) {    
    res.render('index');
});


const hostname = '127.0.0.1';
const port = 4072;
app.listen(port, hostname, () => {
  console.log(`Server for client SPA running at http://${hostname}:${port}/`);
});
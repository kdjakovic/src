const express = require('express');
const auth = require('./04-auth-middleware');

const app = express();

var cors = require('cors');
app.use(cors())

app.use(express.static('public'));
app.set('view engine', 'pug');
app.use(auth.verifyToken);
app.use(express.json()); 



app.post('/login',  function (req, res) {    
    const username = req.body.username;
	const password = req.body.password;
    if (password !== 'some password') {
        res.status(401).send("Invalid username or password");
    }
    else {
        const payload = {
            username
        };
        const token = auth.createToken(payload);
        res.json(token);
    }
});

app.get('/protected', auth.authenticationNeeded, function (req, res) {      
    const username = req.user.username;      
                   
    if (username.toLowerCase() === 'alice' || username.toLowerCase() === 'bob') {
        const data = {  
            'CurrentTime' : Date.now(),          
            'Message' : `Welcome ${username}`
        };
        res.json(data);
    }
    else {
        res.status(403);
        res.end('Forbidden for ' + username);
    }              
});

const hostname = '127.0.0.1';
const port = 4041;
app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
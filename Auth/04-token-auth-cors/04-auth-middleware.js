const jwt = require("jsonwebtoken");
const dotenv = require('dotenv');
dotenv.config();

function verifyToken(req, res, next) {
  req.user = {isAuthenticated : false };  
  let token = req.headers.authorization?.replace(/^Bearer /, '');   
  if (token) {    
    try {      
      token = jwt.verify(token, process.env.TOKEN_KEY);           
      req.user.isAuthenticated = true;
      req.user.username = token.username;
    } 
    catch (err) {     
      const message = err instanceof jwt.TokenExpiredError ? "Expired token" : "Invalid token";
      return res.status(401).send(message);
    }
  }
  return next();
}

function authenticationNeeded(req, res, next) {
  if (!req.user.isAuthenticated) {
    res.writeHead(401, { 'WWW-Authenticate': 'Bearer'});    
    res.end('Authentication is needed');
  }
  else {
    next()
  }
}


function createToken(payload) {
    return jwt.sign(payload, process.env.TOKEN_KEY, { expiresIn : "2m"}); //2 minutes
}

module.exports = {verifyToken, createToken, authenticationNeeded };
const express = require('express');
const app = express();
app.set('view engine', 'pug')

app.get('/', function (req, res) {    
    res.render('client');
});


const hostname = '127.0.0.1';
const port = 4042;
app.listen(port, hostname, () => {
  console.log(`Server for client application running at http://${hostname}:${port}/`);
});
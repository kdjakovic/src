const realm = "FER-Web2 Examples";
function authenticationNeeded(req, res, next) {
    if (!req.user.isAuthenticated) { //user (if authenticated) should be set using getUserInfo method below
        res.writeHead(401, { 'WWW-Authenticate': 'Basic realm="' + realm + '"' });    
        res.end('Authentication is needed');
    }
    else {
        next();
    }
}

function getUserInfo(req, res, next) {
    req.user = {isAuthenticated : false };    

    if (req.headers.authorization) {
        let data = req.headers.authorization.replace(/^Basic /, '');        
        data = Buffer.from(data,'base64').toString('utf8');
        
        const loginInfo = data.split(':');
        const username = loginInfo[0];
        const password = loginInfo[1];        
        if (password === 'some password') {
            req.user.isAuthenticated = true;
            req.user.username = username;            
        }
        else { //invalid username or password
            authenticationNeeded(req, res, next);
            return;
        }
    }
    next();    
}

module.exports = {getUserInfo, authenticationNeeded };
const crypt = require('crypto');

const realm = "FER-Web2 Examples";
const hash = md5Digest(realm);
function md5Digest(data) {
    return crypt.createHash('md5').update(data).digest('hex');
}

function authenticationNeeded(req, res, next) {
    if (!req.user.isAuthenticated) { //user (if authenticated) should be set using getUserInfo method below
        res.writeHead(401, { 'WWW-Authenticate': `Digest realm="${realm}",qop="auth",nonce="${Math.random()}",opaque="${hash}"` });    
        res.end('Authentication is needed');
    }
    else {
        next();
    }
}

function getUserInfo(req, res, next) {
    req.user = {isAuthenticated : false };

    if (req.headers.authorization) {
        const authString = req.headers.authorization.replace(/^Digest /, '');        
        const authData = parseAuthenticationString(authString);
        const username = authData.username;    
        //all users has 'some password' as password
        const ha1 = md5Digest(`${username}:${realm}:some password`);
        const ha2 = md5Digest(`${req.method}:${authData.uri}`);
        const calculatedResponse = md5Digest(`${ha1}:${authData.nonce}:${authData.nc}:${authData.cnonce}:${authData.qop}:${ha2}`);
        
        if (calculatedResponse !== authData.response) { //invalid digest
            authenticationNeeded(req, res, next);
            return;
        }         
        else{
            req.user.isAuthenticated = true;
            req.user.username = username;                                              
        }
    }        
    next();
}

function parseAuthenticationString(authString) {
    var authData = {};
    authString.split(", ").forEach(function(pair) {
        const arr = pair.split('=');
        authData[arr[0]] = arr[1].replace(/"/g, '');
    });    
    return authData;
}

module.exports = {getUserInfo, authenticationNeeded };
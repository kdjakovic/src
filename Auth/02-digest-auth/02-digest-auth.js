const express = require('express');
const app = express();

const auth = require('./02-auth-middleware'); //This line and app port are only differences comparing to 01-basic-auth.js
app.use(auth.getUserInfo);

app.use(express.static('public'));
app.set('view engine', 'pug');

app.get('/', function (req, res) {
    res.render('index', {user : req.user});
});

app.get('/private', auth.authenticationNeeded,  function (req, res) {  
    const username = req.user.username;

    if (username.toLowerCase() === 'alice' || username.toLowerCase() === 'bob') {
        res.render('private', {username : username});
    }
    else {
        res.status(403);
        res.end('Forbidden for ' + username);
    }             
});

const hostname = '127.0.0.1';
const port = 4020;
app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
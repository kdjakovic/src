const express = require('express');
const jwt = require('express-jwt');
const jwks = require('jwks-rsa');
const axios = require('axios');
const app = express();

const dotenv = require('dotenv');
dotenv.config();

var cors = require('cors');
app.use(cors());

app.use(express.static('public'));
app.set('view engine', 'pug')

const iss = 'https://fer-web2.eu.auth0.com';
var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `${iss}/.well-known/jwks.json`
  }),
  audience: `FER-Web2 WebAPI`,
  issuer: `${iss}/`,
  algorithms: ['RS256']
});

app.get('/protected', jwtCheck,  async function (req, res) { 
  let user = req.user;  
  const bearer = req.headers.authorization;
  
  try{
    const res = await axios.post(`${iss}/userinfo`, {},  {headers : {
                                                        Authorization : bearer
                                                    }}); 
     user = res.data;                   
  }
  catch(err) {
    console.log(error);
  }
 
  res.json(JSON.stringify(user));
});

const hostname = '127.0.0.1';
const port = 4091; 
app.listen(port, hostname, () => {
  console.log(`Web API running at http://${hostname}:${port}/`);
});
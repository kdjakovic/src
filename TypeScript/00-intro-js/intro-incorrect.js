var data = require("./recipes.json");

function load() {
    let recipes = data.map(function (item, index)  {
        return {
            id: index + 1, 
            ...item, 
            recipeYield : Number(item.recipeYield),
            ingredients : item.ingredients ? item.ingredients.split("\n") : []
        }
    });
    return recipes;
}

function containsAllSubstrings(haystacks, needles) {
    return needles.every(n => haystacks.some(h => h.toUpperCase().indexOf(n.toUpperCase()) != -1));
}

function findRecipes(yieldPredicate, ...ingredients) {
    let recipes = load();
    return recipes.filter(yieldPredicate)
                  .filter(r => containsAllSubstrings(r.ingredients, ingredients));   
}

let yieldPredicate = y => y >= 3 && y <= 6;
let recipes = findRecipies(yieldPredicate, "Eggs", "Onion");
recipes.forEach(r => {
    console.log(`${r.id}. ${r.name} ${r.url} ${r.publishDate}`)
});

